import sys
import math
import networkx as nx
import matplotlib.pyplot as plt

FILENAME_AIRPORTS = "france.geo"
FILE_SOL_CONCORDE = "france.sol"
FILE_SOL_LKH = "france.lkh"

def lsex2lgeo(x):
    ''' function to convert sexagesimal lat/long in degrees to geographical lat/long in radian ''' 
    degrees = int(x)
    minutes = x - degrees
    return math.pi * (degrees + 5.0 * minutes /  3.0) / 180.0

class Airport():
    nb = 0
    def __init__(self, name, x, y):
        self.name = name # name of airports
        self.id = Airport.nb
        Airport.nb += 1
        self.x = x # sexagesimal latitude in degrees
        self.y = y # sexagesimal longitude in degrees
        self.lat = lsex2lgeo(x)  # geographical latitude in radian
        self.long = lsex2lgeo(y) # geographical longitude in radian

    def distance(self, other):
        ''' Earth distance between two airports in kilometers '''
        RRR = 6378.388
        q1 = math.cos( self.long - other.long )
        q2 = math.cos( self.lat - other.lat )
        q3 = math.cos( self.lat + other.lat )
        return int(RRR * math.acos( 0.5 * ((1.0+q1)*q2 - (1.0-q1)*q3) ) + 1.0)
    
    def __repr__(self):
        return "{0.id}".format(self)

def readAirport(filename):
    airports = []
    # read coordinates of airports
    with open(filename, 'r') as file:
        for line in file:
            name,xs,ys = list(line.split())
            x = float(xs)
            y = float(ys)
            airports.append(Airport(name, x, y))
    return airports


def buildGraph(airports):
    ''' build a complete undirected graph with edge weights equal to Earth distances'''
    g = nx.Graph()
    n = len(airports)
    g.add_nodes_from(range(n))
    g.add_weighted_edges_from([(i, j, airports[i].distance(airports[j])) for i in range(n) for j in range(i+1,n)])
    return g

def length_tour(g, tour):
    edges = [(tour[i-1], tour[i]) for i, ti in enumerate(tour)]
    return sum(g.edges[e]["weight"] for e in edges)
    
def draw_tour(g, tour, airports):
    positions = {i:(airports[i].lat, airports[i].long)for i in tour}
    edges = [(tour[i-1], tour[i]) for i, ti in enumerate(tour)]
    SG = G.edge_subgraph(edges)
    nx.draw(SG, pos=positions, with_labels=True, font_weight= 'bold')
    
def draw_tree(tree, airports):
    positions = {i:(airports[i].lat, airports[i].long) for i in tree.nodes()}
    nx.draw(tree, pos=positions, with_labels=True, font_weight= 'bold')
    
def read_sol_LKH(filename):
    sol = []
    with open(filename, 'r') as f:
        for i, line in enumerate(f):
            try:
                v = int(line)
                if v > 0: sol.append(v-1)
            except ValueError: pass 
    return sol
    
def read_sol_concorde(filename):
    sol = []
    with open(filename, 'r') as f:
        for i, line in enumerate(f):
            if i > 0:
                for w in line.split():
                    sol.append(int(w))
    return sol
    
if __name__ == "__main__":
    airports = readAirport(FILENAME_AIRPORTS)
    G = buildGraph(airports)
    print('number of airports: {}'.format(len(airports)))
    
    ### find a lower bound of an optimal tour
    #print 'minimum tour is greater than ... 
    
    ### build an approximate tour
    
