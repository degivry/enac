
import sys
import networkx as nx
import matplotlib.pyplot as plt

################################## compute if there is an intersection between two routes A-B and C-D
def getIntersectionPoint(_A_x, _A_y, _B_x, _B_y, _C_x, _C_y, _D_x, _D_y, m = 0., k = 0., diviseur = 1., P_x = 0., P_y = 0.):
    if ((_A_x==_C_x and _A_y==_C_y) or  (_A_x==_D_x and _A_y==_D_y) or (_B_x==_C_x and _B_y==_C_y) or (_B_x==_D_x and _B_y==_D_y)):
        return False

    I_x = _B_x - _A_x
    I_y = _B_y - _A_y
    J_x = _D_x - _C_x
    J_y = _D_y - _C_y
    diviseur = (I_x * J_y - I_y * J_x)
 
    if(diviseur == 0):
        return False
    m = (I_x * _A_y - I_x * _C_y - I_y * _A_x + I_y * _C_x ) / diviseur
    k = (J_x * _A_y - J_x * _C_y - J_y * _A_x + J_y * _C_x ) / diviseur
    P_x = _C_x + m * J_x
    P_y = _C_y + m * J_y
    if ((P_x <= _A_x and P_x <= _B_x) or (P_x >= _A_x and P_x >= _B_x) or (P_y <= _A_y and P_y <= _B_y) or (P_y >= _A_y and P_y >= _B_y)):
        return False
    if ((P_x <= _C_x and P_x <= _D_x) or (P_x >= _C_x and P_x >= _D_x) or (P_y <= _C_y and P_y <= _D_y) or (P_y >= _C_y and P_y >= _D_y)):
        return False
    return True


# read airport positions
def read_airports(filename):
    x = {} # latitude of airports
    y = {} # longitude of airports
    with open(filename, "r") as file:
        for line in file:
            line = line.split()
            name,xs,ys = list(line)
            x[name]=float(xs)
            y[name]=float(ys)
    return x, y

# read direct flight planning
def read_routes(filename):
    routes = {} # list of different routes
    with open(filename, "r") as file:
        for line in file:
            ident,source,destination,departuretime,arrivaltime = list(line.split())
            key = (source, destination) if (source < destination) else (destination, source)
            routes[key] = 1
    return routes

def buildGraph(routes, x, y):
    n = len(routes)
    # associate an identification number to every route
    order = list(sorted(routes))

    # create the intersection graph
    G = nx.Graph()
    G.add_nodes_from(range(n))
    for i,r1 in enumerate(order):
        for j,r2 in enumerate(order):
            if (i < j):
                if getIntersectionPoint(x[r1[0]],y[r1[0]],x[r1[1]],y[r1[1]],x[r2[0]],y[r2[0]],x[r2[1]],y[r2[1]]):
                    G.add_edge(i,j)
    return G
    
def build_minizinc_file(filename_routes, G, colormax, allCliques=False, oneCliquePerNode=False, sumcoloring=False):
    # output the associated coloring problem
    output = filename_routes.replace(".txt",f"_{colormax}.mzn")
    with open(output, 'w') as file :
        file.write("%coloring flights\n")
        file.write('include "globals.mzn";\n')
        for v in G.nodes():
            file.write(f"var 0..{colormax-1}: x{v};\n")
        for i,j in G.edges():
            file.write(f"constraint x{i} != x{j};\n")
        
        # add cliques here ...
        
        
        file.write("solve satisfy;\n")

        strliste = ",".join([f'"\\t x{v}=",show(x{v})' for v in G.nodes()])
        file.write(f"output [{strliste}];\n")
    

################################### main program
if __name__ == "__main__":
    n = len(sys.argv)
    if n < 3 or n > 4:
        print("Usage   : python flights.py <filename_airports> <filename_routes> [<colormax=100>]")
        print("Example : python flights.py airports.geo flightsA01.txt")
    else :
        filename_airports = sys.argv[1]
        filename_routes = sys.argv[2]
        colormax = 100 if n != 4 else int(sys.argv[3]) # number of colors
        x, y = read_airports(filename_airports) # latitude/longitude of airports
        routes = read_routes(filename_routes)  # list of different routes
        n = len(routes)
        print(f"found {n} different routes")
        G = buildGraph(routes, x, y)
    
        nx.draw(G)  # make this line a comment to skip it
        plt.show()  # make this line a comment to skip it

        #print("lower bound on chromatic number is ...)
        #print("upper bound on chromatic number is ...)


        
        # DSATUR glouton ....
        

        # modif de build_minizinc_file(...)
        


