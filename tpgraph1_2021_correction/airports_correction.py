import sys
import math
import networkx as nx
import matplotlib.pyplot as plt

FILENAME_AIRPORTS = "france.geo"
FILE_SOL_CONCORDE = "france.sol"
FILE_SOL_LKH = "france.lkh"

def lsex2lgeo(x):
    ''' function to convert sexagesimal lat/long in degrees to geographical lat/long in radian ''' 
    degrees = int(x)
    minutes = x - degrees
    return math.pi * (degrees + 5.0 * minutes /  3.0) / 180.0

class Airport():
    nb = 0
    def __init__(self, name, x, y):
        self.name = name # name of airports
        self.id = Airport.nb
        Airport.nb += 1
        self.x = x # sexagesimal latitude in degrees
        self.y = y # sexagesimal longitude in degrees
        self.lat = lsex2lgeo(x)  # geographical latitude in radian
        self.long = lsex2lgeo(y) # geographical longitude in radian

    def distance(self, other):
        ''' Earth distance between two airports in kilometers '''
        RRR = 6378.388
        q1 = math.cos( self.long - other.long )
        q2 = math.cos( self.lat - other.lat )
        q3 = math.cos( self.lat + other.lat )
        return int(RRR * math.acos( 0.5 * ((1.0+q1)*q2 - (1.0-q1)*q3) ) + 1.0)
    
    def __repr__(self):
        #return "{0.name}:({0.x},{0.y})".format(self)
        return "{0.id}".format(self)

def readAirport(filename):
    airports = []
    # read coordinates of airports
    with open(filename, 'r') as file:
        for line in file:
            name,xs,ys = list(line.split())
            x = float(xs)
            y = float(ys)
            airports.append(Airport(name, x, y))
    return airports


def buildGraph(airports):
    ''' build a complete undirected graph with edge weights equal to Earth distances'''
    g = nx.Graph()
    n = len(airports)
    #g.add_nodes_from(airports)
    #g.add_weighted_edges_from([(a1, a2, a1.distance(a2)) for a1 in airports for a2 in airports])
    g.add_nodes_from(range(n))
    g.add_weighted_edges_from([(i, j, airports[i].distance(airports[j])) for i in range(n) for j in range(i+1,n)])
    return g

def length_tour(g, tour):
    edges = [(tour[i-1], tour[i]) for i, ti in enumerate(tour)]
    return sum(g.edges[e]["weight"] for e in edges)
    
def draw_tour(g, tour, airports):
    positions = {i:(airports[i].lat, airports[i].long)for i in tour}
    edges = [(tour[i-1], tour[i]) for i, ti in enumerate(tour)]
    SG = G.edge_subgraph(edges)
    nx.draw(SG, pos=positions, with_labels=True, font_weight= 'bold')
    
def draw_tree(tree, airports):
    positions = {i:(airports[i].lat, airports[i].long) for i in tree.nodes()}
    nx.draw(tree, pos=positions, with_labels=True, font_weight= 'bold')
    
def read_sol_LKH(filename):
    sol = []
    with open(filename, 'r') as f:
        for i, line in enumerate(f):
            try:
                v = int(line)
                if v > 0: sol.append(v-1)
            except ValueError: pass 
    return sol
    
def read_sol_concorde(filename):
    sol = []
    with open(filename, 'r') as f:
        for i, line in enumerate(f):
            if i > 0:
                for w in line.split():
                    sol.append(int(w))
    return sol
    
def build_approximate_tour(G):
    tour = [] # initialize an empty tour
    tour.append(0) # start tour at airport 0
    objective = 0 # corresponding tour distance
    # append nearest unvisited neighbor until all airports have been added to tour
    n = G.number_of_nodes()
    unvisited_neighbors = list(range(1,n))
    v = 0
    while len(unvisited_neighbors) != 0:
        m, u = min((G.edges[v,u]['weight'],u) for u in unvisited_neighbors)
        objective += m
        tour.append(u)
        unvisited_neighbors.remove(u)
        v = u
    objective += G.edges[v,0]['weight']
    return tour, objective

if __name__ == "__main__":
    airports = readAirport(FILENAME_AIRPORTS)
    G = buildGraph(airports)
    print('number of airports: {}'.format(len(airports)))
    lowerbound = True
    upperbound = True
    concorde = True
    lkh = True
    
    ### find a lower bound of an optimal tour
    #print 'minimum tour is greater than ... 
    if lowerbound :
        T = nx.minimum_spanning_tree(G)
        lower_bound = sum(T.edges[e]['weight'] for e in T.edges)
        print('minimum tour is greater than', lower_bound)
        draw_tree(T, airports)
        plt.show()
    
    ### build an approximate tour
    if upperbound :
        tour, objective = build_approximate_tour(G)
        l = length_tour(G, tour)
        print("nearest neighbor tour = ", l)#, tour, objective)
        draw_tour(G, tour, airports)
        plt.show()
    
    if concorde :
        sol_concorde = read_sol_concorde(FILE_SOL_CONCORDE)
        l_concorde = length_tour(G, sol_concorde)
        print("concorde = ", l_concorde)#, sol_concorde)
        draw_tour(G, sol_concorde, airports)
        plt.show()

    if lkh :
        sol_lkh = read_sol_LKH(FILE_SOL_LKH)
        l_lkh = length_tour(G, sol_lkh)
        print("LKH = ", l_lkh)#, sol_lkh)
        draw_tour(G, sol_lkh, airports)
        plt.show()
    
